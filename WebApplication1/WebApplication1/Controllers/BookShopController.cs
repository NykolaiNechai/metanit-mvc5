﻿using System;
using System.IO;
using System.Web.Mvc;
using WebApplication1.Util;

namespace WebApplication1.Controllers
{
    public class BookShopController : Controller
    {
        public ActionResult Index()
        {
            HttpContext.Response.Write("<h1>Hello World</h1>");

            var browser = HttpContext.Request.Browser.Browser;
            var userAgent = HttpContext.Request.UserAgent;
            var url = HttpContext.Request.RawUrl;
            var ip = HttpContext.Request.UserHostAddress;
            var referrer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;

            return Content(string.Concat(browser, userAgent, url, ip, referrer));
        }

        public ActionResult GetHtml()
        {
            return new MyHtmlResult("<h2>Hellow world</h2>");
        }

        public ActionResult GetImage()
        {
            return new MyImageResult("../Images/visualstudio.png");
        }

        public RedirectResult SomeTmpRedirect()
        {
            return Redirect("/home/index");
        }

        public RedirectResult SomeConstantRedirect()
        {
            return RedirectPermanent("/home/index");
        }

        public RedirectToRouteResult SomeRedirectToRoute()
        {
            return RedirectToRoute(new {controller = "Home", action = "Index" });
        }

        public RedirectToRouteResult SomeRedirectToAction()
        {
            return RedirectToAction("Square", "Home", new { a = 10, b = 3 });
        }

        public ActionResult Square()
        {
            try
            {
                int a = int.Parse(Request.Params["a"]);
                int b = int.Parse(Request.Params["b"]);

                var s = a * b / 2.0;

                return Content(string.Concat("<h2>Площадь треугольника с основанием ", a, " и высотой ", b, " равна ", s, "</h2>"));
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(404);
            }
        }

        public FileResult GetFile()
        {
            var filePath = Server.MapPath("~/Files/PdfIcon.pdf");
            var fileType = "application/pdf";
            var fileName = "pdfIcon.pdf";

            return File(filePath, fileType, fileName);
        }

        public FileResult GetFileBytes()
        {
            var filePath = Server.MapPath("~/Files/PdfIcon.pdf");
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);

            var fileType = "application/pdf";
            var fileName = "pdfIcon.pdf";

            return File(bytes, fileType, fileName);
        }

        public FileResult GetFileStream()
        {
            var filePath = Server.MapPath("~/Files/PdfIcon.pdf");
            FileStream fs = new FileStream(filePath, FileMode.Open);

            var fileType = "application/pdf";
            var fileName = "pdfIcon.pdf";

            return File(fs, fileType, fileName);
        }
    }
}