﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class BookDbInitializer : DropCreateDatabaseAlways<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            for(int i = 0; i < 2; i++)
            {
                context.Books.Add(new Book {Name = string.Format("Война и мир, том {0}", i), Author = "Л. Толстой", Price = 220 + i});
                context.Books.Add(new Book { Name = string.Format("Отцы и дети, том {0}", i), Author = "И.Тургенев", Price = 180 + i });
                context.Books.Add(new Book { Name = string.Format("Пандора, том {0}", i), Author = "А. Чехов", Price = 150 + i });
            }
            base.Seed(context);
        }
    }
}