﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Util
{
    public class MyHtmlResult : ActionResult
    {
        private string _htmlCode;

        public MyHtmlResult(string html)
        {
            _htmlCode = html;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            string fullHtmlCode = "<!DOCTYPE html><html><head>";
            fullHtmlCode += "<title>main page</title>";
            fullHtmlCode += "<meta charset=utf-8/>";
            fullHtmlCode += "</head><body>";

            fullHtmlCode += _htmlCode;

            fullHtmlCode += "<body/></html>";

            context.HttpContext.Response.Write(fullHtmlCode);
        }
    }
}