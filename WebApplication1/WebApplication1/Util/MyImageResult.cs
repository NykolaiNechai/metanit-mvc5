﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Util
{
    public class MyImageResult : ActionResult
    {
        private string _path;

        public MyImageResult(string path)
        {
            _path = path;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var result = "<div style='width:100%;text-align:center;'>" + "<img style='max-width:600px;' src='" + _path + "' /></div>";

            context.HttpContext.Response.Write(result);
        }
    }
}